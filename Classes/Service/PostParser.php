<?php

namespace Hn\PostParser\Service;


class PostParser
{
    /**
     * @var array
     */
    protected $termsReplacementProvider;

    /**
     * @var \TYPO3\CMS\Extbase\SignalSlot\Dispatcher
     * @inject
     */
    protected $signalSlotDispatcher;

    /**
     * @param string $html
     * @return string
     * @throws \TYPO3\CMS\Extbase\SignalSlot\Exception\InvalidSlotException
     * @throws \TYPO3\CMS\Extbase\SignalSlot\Exception\InvalidSlotReturnException
     */
    public function execute($html)
    {
        // the postparser uses DomDocument to parse the html
        // DomDocument does not allow html in script tags which is needed for the ractive templates

        $scriptTagContent = [];
        $dataKey = sha1(self::class);

        // 1. Get script tag content and generate a unique key per element
        $html = preg_replace_callback('#<script(.*?)>(.*?)</script>#is', function (array $matches) use (&$scriptTagContent, $dataKey) {
            $dataIndex = '';
            if (!empty($matches[2])) {
                $scriptTagContent[] = $matches[2];
                $dataIndex = ' data-' . $dataKey . '="' . (count($scriptTagContent) - 1) . '"';
            }
            return implode('', ['<script', $matches[1], $dataIndex, '>', '</script>']);
        }, $html);

        // 2. execute normally
        $this->signalSlotDispatcher->dispatch(self::class, 'onPostParserExecution', [$this]);
        $html = $this->doReplace($html);

        // 3. write the saved script tag contents back to their elements (identified by the unique key)
        $html = preg_replace_callback('#<script(.*?)data-' . $dataKey . '="([0-9]+)"(.*?)></script>#is', function (array $matches) use ($scriptTagContent) {
            return '<script' . $matches[1] . '>' . $scriptTagContent[$matches[2]] . '</script>';
        }, $html);

        return $html;
    }

    /**
     * @param $terms
     * @param $replacementProvider
     */
    public function registerTerms(ReplacementProviderInterface $replacementProvider, $terms)
    {
        foreach ($terms as &$term) {
            $this->termsReplacementProvider[$term] = $replacementProvider;
        }
    }

    /**
     * @param string $html
     * @return string
     */
    private function doReplace($html)
    {
        if (!$this->termsReplacementProvider) {
            return $html;
        }

        libxml_use_internal_errors(true);

        $doc = new \DOMDocument();
        $doc->loadHTML($html);
        $body = $doc->getElementsByTagName('body')->item(0);

        $xPath = new \DOMXPath($doc);

        /**
         * @var string $term
         * @var ReplacementProviderInterface $replacementProvider
         */
        foreach ($this->termsReplacementProvider as $term => $replacementProvider) {

            // 1. find all matches for term
            $xPathQuery = '//text()[contains(., "' . $term . '")]';
            $nodeList = $xPath->query($xPathQuery, $body);
            if (!$nodeList) {
                continue;
            }

            // 2. ask replacement provider for the replacements
            $replacements = $replacementProvider->getReplacements($term, $nodeList);

            // 3. actually replace the terms by their replacement
            foreach ($nodeList as $index => $node) {
                if ($term === $replacements[$index]) {
                    continue;
                }

                $newNode = $doc->createDocumentFragment();
                $nodeValue = $node->nodeValue;

                $strpos = strpos($nodeValue, $term);
                if ($strpos === false) {
                    continue;
                }

                $nodeValue = $this->replaceTerm($replacementProvider, $term, $replacements[$index], $nodeValue);

                $newNode->appendXML($nodeValue);
                $node->parentNode->replaceChild($newNode, $node);
            }
        }

        return $doc->saveHTML();
    }

    /**
     * @param ReplacementProviderInterface $replacementProvider
     * @param string $term
     * @param string $replacement
     * @param string $nodeValue
     * @return string|string[]|null
     */
    private function replaceTerm($replacementProvider, $term, $replacement, $nodeValue)
    {
        $isReplaceSingle = $replacementProvider->isReplaceSingle();
        $isReplaceSubstrings = $replacementProvider->isReplaceSubstrings();

        if ($isReplaceSingle) {
            if ($isReplaceSubstrings) {
                return preg_replace('/(' . $term . ')/', $replacement, $nodeValue, 1);
            } else {
                return preg_replace('/\b(' . $term . ')\b/', $replacement, $nodeValue, 1);
            }
        } else {
            if ($isReplaceSubstrings) {
                return preg_replace('/(' . $term . ')/', $replacement, $nodeValue);
            } else {
                return preg_replace('/\b(' . $term . ')\b/', $replacement, $nodeValue);
            }
        }
    }
}