<?php

namespace Hn\PostParser\Service;

interface ReplacementProviderInterface
{
    /**
     * @param string $term
     * @param \DOMNodeList $nodeList
     * @return array
     */
    public function getReplacements($term, \DOMNodeList $nodeList);

    /**
     * @return bool
     */
    public function isReplaceSingle();

    /**
     * @return bool
     */
    public function isReplaceSubstrings();
}