<?php

$EM_CONF[$_EXTKEY] = [
    'title' => 'Post Parser',
    'description' => 'A post parser extension.',
    'category' => 'plugin',
    'author' => 'Marvin Dosse',
    'author_company' => 'hauptsache.net GmbH',
    'author_email' => 'marvin@hauptsache.net',
    'state' => 'alpha',
    'clearCacheOnLoad' => true,
    'version' => '1.0.6',
    'constraints' => [
        'depends' => [
            'typo3' => '8.7.0-8.9.99',
        ]
    ]
];